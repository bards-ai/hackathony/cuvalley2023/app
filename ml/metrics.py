import numpy as np
from sklearn.metrics import (
    mean_absolute_error,
    mean_absolute_percentage_error,
    mean_squared_error,
    r2_score,
)


def append_metrics(metrics, all_y_test_true, all_y_test_predicted):
    if len(all_y_test_predicted) < 2:  # 1 because of r2
        return metrics

    metrics["mape"].append(
        mean_absolute_percentage_error(all_y_test_true, all_y_test_predicted)
    )
    metrics["mae"].append(mean_absolute_error(all_y_test_true, all_y_test_predicted))
    metrics["mse"].append(mean_squared_error(all_y_test_true, all_y_test_predicted))
    metrics["r2"].append(r2_score(all_y_test_true, all_y_test_predicted))
    return metrics


def save_metrics(metrics, name, residuals=[]):
    macro_mape = np.mean(metrics["mape"])
    macro_mae = np.mean(metrics["mae"])
    macro_mse = np.mean(metrics["mse"])
    macro_r2 = np.mean(metrics["r2"])

    print(f"{macro_mae:.2f}")
    with open(
        f"../data/results/{name}",
        "a+",
        encoding="utf-8",
    ) as f:
        if residuals:
            f.write("Metrics for ALL test-y:\n")

        f.write(f"MAPE {macro_mape:.3f}\n")
        f.write(f"MAE {macro_mae:.3f}\n")
        f.write(f"MSE {macro_mse:.3f}\n")
        f.write(f"R2 {macro_r2:.3f}\n")

        if residuals:
            quantile = np.quantile(residuals, 0.95)
            f.write(f"conformal confidence: {quantile:.2f}\n")
            f.write("#" * 50 + "\n")
            f.write("Metrics for test-y below 3m:\n")
