import pandas as pd
from xgboost import XGBRegressor
from models.single_model import SingleModel
from utils import get_data_for_columns


class AllDataModel(SingleModel):
    def __init__(
        self, n_days_lag: int = 7, frequency: str = "1D", custom_loss_function=None
    ):
        super().__init__(n_days_lag, frequency, custom_loss_function)

    def get_data(self, path="../data/merged.csv"):
        meteo_df = pd.read_csv(path, header=0, index_col=["date"])
        meteo_df.index = pd.to_datetime(meteo_df.index)

        data = meteo_df.loc[:, meteo_df.columns != "water_level_cm_151160060"]
        y = meteo_df.loc[:, meteo_df.columns == "water_level_cm_151160060"]

        return data, y
