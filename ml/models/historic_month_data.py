from xgboost import XGBRegressor
from models.historic_data import HistoricModel


class HistoricMonthModel(HistoricModel):
    def __init__(
        self, n_days_lag: int = 7, frequency: str = "1D", custom_loss_function=None
    ):
        super().__init__(n_days_lag, frequency, custom_loss_function)

    def get_data(self, path="../data/merged.csv"):
        data, y = super().get_data(path)
        data["month"] = data.index.month
        return data, y
