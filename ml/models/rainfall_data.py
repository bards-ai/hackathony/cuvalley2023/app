from xgboost import XGBRegressor
from models.single_model import SingleModel
from utils import get_data_for_columns


class RainfallModel(SingleModel):
    def __init__(
        self, n_days_lag: int = 7, frequency: str = "1D", custom_loss_function=None
    ):
        super().__init__(n_days_lag, frequency, custom_loss_function)

    def get_data(self, path="../data/merged.csv"):
        lags = list(range(1, self.N_DAYS_LAG + 1)) + [14]
        baseline_columns = [f"water_level_cm_151160060_{i}" for i in lags]

        lags += ["std", "mean"]
        kghm_columns = [
            f"{name}_rainfall_mm_{i}"
            for i in lags
            for name in ["251160080", "351160415", "251160190", "251170420", "251160110"]
        ]

        kghm_columns += baseline_columns
        data, y = get_data_for_columns(kghm_columns, path)
        return data, y