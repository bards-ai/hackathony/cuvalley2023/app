import pandas as pd
from itertools import product
from xgboost import XGBRegressor
from tqdm import tqdm
from models.interface import Model


class EnsembleModel(Model):
    def __init__(
        self,
        features: list = ["flow", "water_level_cm"],
        n_days_lag: int = 7,
        frequency: str = "1D",
        custom_loss_function=None,
    ):
        self.N_DAYS_LAG = n_days_lag
        self.FREQUENCY = frequency

        self.locations = [
            "149180060",
            "149180070",
            "150160280",
            "150170090",
        ]

        self.features = features
        self.custom_loss_function = custom_loss_function
        self.lags = list(range(1, self.N_DAYS_LAG + 1)) + [14]
        self.models = {}
        self.main_model = None

        self.location_features = [
            f"{feature}_{location}"
            for feature, location in product(self.features, self.locations)
        ]
        self.location_features.remove("flow_150170090")

    def _get_main_model_features(self, data):
        main_features_columns = [
            f"{feature}_{location}_{lag}"
            for feature, location, lag in product(
                self.features, self.locations, self.lags
            )
        ]

        historic_features_columns = [
            f"water_level_cm_151160060_{lag}" for lag in self.lags
        ]

        main_features_columns += historic_features_columns

        return data.loc[:, data.columns.isin(main_features_columns)]

    def _initiate_single_model(self, use_custom_loss):
        if use_custom_loss and self.custom_loss_function:
            return XGBRegressor(n_jobs=-1, objective=self.custom_loss_function())
        return XGBRegressor(n_jobs=-1)

    def get_data(self, path="../data/merged.csv"):
        meteo_df = pd.read_csv(path, header=0, index_col=["date"])
        meteo_df.index = pd.to_datetime(meteo_df.index)

        data = meteo_df.loc[:, meteo_df.columns != "water_level_cm_151160060"]
        y = meteo_df.loc[:, meteo_df.columns == "water_level_cm_151160060"]

        return data, y

    def fit(self, data, y_main):
        for y_column in tqdm(self.location_features):
            features_columns = [f"{y_column}_{lag}" for lag in self.lags]
            X_train = data.loc[:, data.columns.isin(features_columns)]
            y_train = data.loc[:, data.columns == y_column]

            model = self._initiate_single_model(use_custom_loss=False)
            model.fit(X_train, y_train)
            self.models[y_column] = model
            print(f"Model for {y_column} fitted")

        self.main_model = self._initiate_single_model(use_custom_loss=True)

        X_train = self._get_main_model_features(data)
        y_train = y_main
        self.main_model.fit(X_train, y_train)

    def predict(self, X, test_date):
        X_test = X[X.index == test_date]

        for y_column in self.location_features:
            features_columns = [f"{y_column}_{lag}" for lag in self.lags]
            features = X_test.loc[:, X_test.columns.isin(features_columns)]
            feature_prediction = self.models[y_column].predict(features)

            for i in range(1, self.N_DAYS_LAG + 1):
                X.loc[
                    test_date + pd.Timedelta(self.FREQUENCY) * i,
                    f"{y_column}_{i}",
                ] = feature_prediction[0]

            X.loc[
                test_date + pd.Timedelta(self.FREQUENCY) * 14,
                f"{y_column}_14",
            ] = feature_prediction[0]

        X_test = self._get_main_model_features(X).loc[X.index == test_date]
        return self.main_model.predict(X_test)
