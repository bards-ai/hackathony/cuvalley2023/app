from abc import abstractmethod
from xgboost import XGBRegressor
from models.interface import Model


class SingleModel(Model):
    def __init__(
        self, n_days_lag: int = 7, frequency: str = "1D", custom_loss_function=None
    ):
        self.N_DAYS_LAG = n_days_lag
        self.FREQUENCY = frequency
        self.custom_loss_function = custom_loss_function

    @abstractmethod
    def get_data(self, path="../data/merged.csv"):
        pass 

    def fit(self, X_train, y_train):
        if self.custom_loss_function:
            self.model = XGBRegressor(n_jobs=-1, objective=self.custom_loss_function())
        else:
            self.model = XGBRegressor(n_jobs=-1)
        self.model.fit(X_train, y_train)

    def predict(self, X, test_date):
        X_test = X[X.index == test_date]
        return self.model.predict(X_test)
