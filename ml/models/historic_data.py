from xgboost import XGBRegressor
from models.single_model import SingleModel
from utils import get_data_for_columns


class HistoricModel(SingleModel):
    def __init__(
        self, n_days_lag: int = 7, frequency: str = "1D", custom_loss_function=None
    ):
        super().__init__(n_days_lag, frequency, custom_loss_function)

    def get_data(self, path="../data/merged.csv"):
        lags = list(range(1, self.N_DAYS_LAG + 1)) + [14]
        baseline_columns = [f"water_level_cm_151160060_{i}" for i in lags]

        data, y = get_data_for_columns(baseline_columns, path)
        return data, y
