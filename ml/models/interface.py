import pandas as pd
from abc import ABC, abstractmethod


class Model(ABC):
    @abstractmethod
    def get_data(self, path: str) -> None:
        pass

    @abstractmethod
    def fit(self, data: pd.DataFrame, y_main: pd.DataFrame) -> None:
        pass

    @abstractmethod
    def predict(self, X: pd.DataFrame, test_date: pd.Timestamp) -> float:
        pass
