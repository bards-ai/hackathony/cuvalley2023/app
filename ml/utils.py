import pandas as pd

def get_data_for_columns(columns: list, path: str):
    meteo_df = pd.read_csv(path, header=0, index_col=["date"])
    meteo_df.index = pd.to_datetime(meteo_df.index)

    data = meteo_df[columns]
    y = meteo_df.loc[:, meteo_df.columns == "water_level_cm_151160060"]

    return data, y