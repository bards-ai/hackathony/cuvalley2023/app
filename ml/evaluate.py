from collections import defaultdict
import datetime as dt
import logging
import time
import pandas as pd
from tqdm import tqdm

from metrics import append_metrics, save_metrics
from models import (
    AllDataModel,
    HistoricMonthModel,
    RainfallModel,
    HistoricModel,
    Model,
    EnsembleModel,
)
from loss import custom_loss_train


FREQUENCY = "1D"
N_DAYS_LAG = 7
START_DATE = dt.datetime(2016, 1, 1)
TEST_CUTOFF_DATE = dt.datetime(2019, 1, 1)

logger = logging.getLogger()
logging.basicConfig(
    format="%(asctime)s - %(message)s", datefmt="%d-%m-%Y %H:%M:%S", level=logging.INFO
)


def evaluation_loop(model: Model, name: str = ""):
    real_data, y = model.get_data()
    data = real_data.copy()

    # train
    # y_train = y[(START_DATE <= y.index) & (y.index < TEST_CUTOFF_DATE)]
    # X_train = data[(START_DATE <= data.index) & (data.index < TEST_CUTOFF_DATE)]

    # test
    test_cutoff_date = TEST_CUTOFF_DATE

    # logging.info("Fitting model...")
    # model.fit(X_train, y_train)

    metrics = defaultdict(list)
    metrics_below_3m = defaultdict(list)

    residuals = []

    # logging.info("Predicting...")

    for _ in tqdm(range(N_TESTS)):

        y_train = y[(START_DATE <= y.index) & (y.index < test_cutoff_date)]
        X_train = data[(START_DATE <= data.index) & (data.index < test_cutoff_date)]
        model.fit(X_train, y_train)

        all_y_test_predicted = []
        all_y_test_true = []

        for i in range(N_DAYS_PREDICTIONS):
            # true
            y_test_true = y[y.index == test_cutoff_date]
            all_y_test_true.append(y_test_true.iloc[0][0])

            # predict
            y_test_predicted = model.predict(data, test_cutoff_date)
            all_y_test_predicted.append(y_test_predicted[0])

            residuals.append(y_test_predicted[0] - y_test_true.iloc[0][0])
            # update data
            for i in range(1, N_DAYS_LAG + 1):
                data.loc[
                    test_cutoff_date + pd.Timedelta(FREQUENCY) * i,
                    f"water_level_cm_151160060_{i}",
                ] = y_test_predicted[0]

            data.loc[
                test_cutoff_date + pd.Timedelta(FREQUENCY) * 14,
                f"water_level_cm_151160060_14",
            ] = y_test_predicted[0]

            test_cutoff_date += pd.Timedelta(FREQUENCY)

        data = real_data.copy()

        # metrics for all test-y
        append_metrics(metrics, all_y_test_true, all_y_test_predicted)

        # metrics for test-y below 3m
        all_y_test_true_below_3m = [y for y in all_y_test_true if y <= 300]
        all_y_test_predicted_below_3m = [
            y
            for idx, y in enumerate(all_y_test_predicted)
            if all_y_test_true[idx] <= 300
        ]
        append_metrics(
            metrics_below_3m, all_y_test_true_below_3m, all_y_test_predicted_below_3m
        )

        for step, (test, predict) in enumerate(
            zip(all_y_test_true, all_y_test_predicted)
        ):
            upper_bound = 0.75 * step + test
            lower_bound = -0.75 * step + test

            if predict > upper_bound:
                new_predict = predict - upper_bound
            elif upper_bound >= predict >= lower_bound:
                new_predict = -min(upper_bound - predict, predict - lower_bound)
            elif predict < lower_bound:
                new_predict = lower_bound - predict

            residuals.append(new_predict)

    file_name = f"{name}-{str(dt.datetime.now())[11:20]}txt"

    with open(f"../data/results/{name}-residuals.txt", "w") as output:
        output.write(str(residuals))
    
    save_metrics(metrics, file_name, residuals)
    save_metrics(metrics_below_3m, file_name)

    # model.save_model("./data/model_xgb.json")


N_DAYS_PREDICTIONS = 14
N_TESTS = 52

if __name__ == "__main__":
    start = time.time()

    functions = [None, custom_loss_train]
    models = [
        AllDataModel,
        HistoricModel,
        RainfallModel,
        HistoricMonthModel,
        EnsembleModel,
    ]

    for model in models:
        for function in functions:
            if function is None:
                print("NORMAL loss")
            else:
                print("CUSTOM loss")

            print(
                f"{N_DAYS_PREDICTIONS}dni-bards_model-{model.__name__}{'-' + function.__name__ if function else ''}"
            )
            evaluation_loop(
                model=model(custom_loss_function=function),
                name=f"backtesting/{N_DAYS_PREDICTIONS}dni-{model.__name__}{'-' +function.__name__ if function else ''}",
            )

    end = time.time()
    print(f"Finished in {dt.timedelta(seconds=end-start)}")
