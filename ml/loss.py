import numpy as np


def custom_loss_train(rho=10.0):
    # rho is the penalty coefficient

    def loss_function(y_true, y_pred):
        penalty_condition = y_true <= 300
        residuals = (y_true - y_pred).astype("float")

        gradient = np.where(penalty_condition, rho * -2 * residuals, -2 * residuals)
        hessian = np.where(penalty_condition, 2 * rho, 2.0)
        return gradient, hessian

    return loss_function


# to use it convert y_pred(dMatrix to numpy array)
# def custom_loss_val(rho=10.0):
#     def loss_function(y_true, y_pred):
#         residual = (y_true - y_pred).astype("float")
#         penalty_condition = y_true <= 300
#         loss = np.where(penalty_condition, (residual**2) * rho, residual**2)
#         return "custom_loss_val", np.mean(loss), False

#     return loss_function


# def fit_model_custom_loss(X_train, y_train):
#     model = XGBRegressor(n_jobs=-1, objective=custom_loss_train())
#     model.fit(X_train, y_train)
#     return model
