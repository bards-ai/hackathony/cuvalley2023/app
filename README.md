# App


## Projekt

Repozytorium to zawiera model do predykcji ilości wody w rzece oraz serwer umożliwiający korzystanie z niego. Model oparty jest na algorytmie uczenia maszynowego, który został nauczony na podstawie danych historycznych dotyczących poziomu wody w rzece. Dzięki temu jest w stanie przewidywać poziom wody w przyszłości na podstawie danych aktualnych. Serwer umożliwia dostęp do modelu poprzez API, co pozwala na łatwe wykorzystanie go w różnych aplikacjach.

![Alt text](https://media.discordapp.net/attachments/1068603815110131763/1069027306724655154/Screenshot_2023-01-28_at_23.52.15.png)

Korelacje poziomu wody z stacją hydrologiczną w Głogowie.

## Wymagania

- Python 3

## Urchomienie serwera

`pip install -r requirments`
`python3 app.py`

## Uruchomienie badań 
`cd ml && python3 evaluate.py`
``
