import uvicorn
import xgboost as xgb
import datetime as dt
import pandas as pd
import numpy as np

from fastapi import FastAPI
from ml.model import FREQUENCY, N_DAYS_LAG, N_DAYS_PREDICTIONS, get_data
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

model = xgb.XGBRegressor()
model.load_model("./data/model_xgb.json")

real_data, y = get_data(path="./data/merged.csv")
data = real_data.copy()

# from ./data/results/2023-01-28 20:06:27.049901.txt
QUANTILE = 8.16651458740235

def make_json(ground_truth, predictions, predictions_dates, upper_conformal_bounds, lower_conformal_bounds):

    area_with_interval = []
    for i in range(N_DAYS_PREDICTIONS):
        area_with_interval.append({'x': str(ground_truth.index[i].to_pydatetime())[:10], 'y' : [format(float(ground_truth.iloc[i][0]),'.2f'), format(float(ground_truth.iloc[i][0]),'.2f')]})

    for i in range(N_DAYS_PREDICTIONS):
        area_with_interval.append({'x': str(predictions_dates[i])[:10], 'y' : [format(float(lower_conformal_bounds[i]),'.2f'), format(float(upper_conformal_bounds[i]),'.2f')]})

    ground_truth_line = [{'x': str(ground_truth.index[i].to_pydatetime())[:10], 'y' : format(float(ground_truth.iloc[i][0]),'.2f')} for i in range(len(ground_truth))]
    predictions_line = ground_truth_line[:N_DAYS_PREDICTIONS] + [{'x': str(predictions_dates[i])[:10], 'y' : format(float(predictions[i]),'.2f')} for i in range(len(predictions))]

    return {
        "area": area_with_interval,
        "predictions": predictions_line,
        "true": ground_truth_line,

    }

# http://localhost:8123/predict?year=2019&month=1&day=10
@app.get("/predict")
async def predict(year: int,month: int, day: int):

    start_date = dt.datetime(year, month, day)

    ground_truth_before = y.loc[start_date - pd.Timedelta(FREQUENCY) * N_DAYS_PREDICTIONS:start_date]
    ground_truth_after = y.loc[start_date + pd.Timedelta(FREQUENCY): start_date + pd.Timedelta(FREQUENCY) * (N_DAYS_PREDICTIONS - 1)]
    ground_truth = pd.concat([ground_truth_before, ground_truth_after])

    predictions = []
    predictions_dates = []
    upper_conformal_bounds = []
    lower_conformal_bounds = []

    data = real_data.copy()

    for day in range(N_DAYS_PREDICTIONS):
        X_test = data[data.index == start_date]
        y_test_predicted = model.predict(X_test)
        predictions.append(y_test_predicted[0])
        predictions_dates.append(start_date)

        # update data
        for i in range(1, N_DAYS_LAG + 1):
            data.loc[
                start_date + pd.Timedelta(FREQUENCY) * i,
                f"water_level_cm_151160060_{i}",
            ] = y_test_predicted[0]


        start_date += pd.Timedelta(FREQUENCY)

        upper_bound = 0.75 * day + QUANTILE + y_test_predicted[0]
        lower_bound = (-0.75 * day) - QUANTILE + y_test_predicted[0]

        upper_conformal_bounds.append(upper_bound)
        lower_conformal_bounds.append(lower_bound)

    result = make_json(ground_truth, predictions, predictions_dates, upper_conformal_bounds, lower_conformal_bounds)
    return result

hydro_data = pd.read_csv('./data/hydro.csv')
hydro_data['date'] = pd.to_datetime(hydro_data['date'], format='%Y-%m-%d')
hydro_data = hydro_data.sort_values(by='date')
#hydro_data = hydro_data.set_index('date')

stations = [{ 'position': [52.03305555555556, 15.6075], 'name': 'CIGACICE', 'value': 0.8 },
    {
        'position': [51.80222222222222, 15.721666666666668],
        'name': 'NOWA SÓL',
        'value': 0.8
    },
    { 'position': [52.09, 16.644166666666667], 'name': 'KOŚCIAN', 'value': 0.8 },
    {
        'position': [51.40888888888889, 16.443333333333335],
        'name': 'ŚCINAWA',
        'value': 0.8
    },
    {
        'position': [51.63166666666667, 16.46222222222222],
        'name': 'OSETNO',
        'value': 0.8
    },
    {
        'position': [51.782777777777774, 16.66888888888889],
        'name': 'RYDZYNA',
        'value': 0.8
    },
    {
        'position': [51.67388888888889, 16.059166666666666],
        'name': 'GŁOGÓW',
        'value': 0.8
    },
    {
        'position': [51.45361111111112, 16.95611111111111],
        'name': 'KANCLERZOWICE',
        'value': 0.8
    },
    {
        'position': [51.49305555555556, 17.14638888888889],
        'name': 'ŁĄKI',
        'value': 0.8
    },
    {
        'position': [51.51611111111111, 17.56416666666667],
        'name': 'BOGDAJ',
        'value': 0.8
    },
    {
        'position': [51.22611111111112, 16.49277777777778],
        'name': 'MALCZYCE',
        'value': 0.8
    },
    {
        'position': [51.22777777777778, 16.199166666666667],
        'name': 'PIĄTNICA',
        'value': 0.8
    },
    {
        'position': [51.24888888888889, 16.14472222222222],
        'name': 'RZESZOTARY',
        'value': 0.8
    },
    {
        'position': [51.13944444444444, 16.025555555555556],
        'name': 'RZYMÓWKA',
        'value': 0.8
    },
    {
        'position': [51.111666666666665, 16.09361111111111],
        'name': 'WINNICA',
        'value': 0.8
    },
    {
        'position': [51.04833333333333, 16.189722222222223],
        'name': 'JAWOR',
        'value': 0.8
    },
    {
        'position': [51.01777777777777, 15.887777777777778],
        'name': 'ŚWIERZAWA',
        'value': 0.8
    },
    {
        'position': [51.272777777777776, 15.945555555555556],
        'name': 'CHOJNÓW',
        'value': 0.8
    },
    {
        'position': [51.18888888888888, 15.863888888888887],
        'name': 'ZAGRODNO',
        'value': 0.8
    },
    {
        'position': [51.26055555555556, 16.726666666666667],
        'name': 'BRZEG DOLNY',
        'value': 0.8
    },
    { 'position': [51.08027777777778, 17.71], 'name': 'NAMYSŁÓW', 'value': 0.8 },
    {
        'position': [51.121944444444445, 16.842777777777776],
        'name': 'JARNOŁTÓW',
        'value': 0.8
    },
    {
        'position': [51.086666666666666, 16.7925],
        'name': 'BOGDASZOWICE',
        'value': 0.8
    },
    { 'position': [50.95305555555556, 16.4925], 'name': 'ŁAŻANY', 'value': 0.8 },
    {
        'position': [50.87777777777778, 16.233055555555556],
        'name': 'CHWALISZÓW',
        'value': 0.8
    },
    {
        'position': [50.86027777777778, 16.31361111111111],
        'name': 'ŚWIEBODZICE',
        'value': 0.8
    },
    {
        'position': [50.96833333333333, 16.650555555555552],
        'name': 'MIETKÓW',
        'value': 0.8
    },
    {
        'position': [50.91555555555556, 16.583888888888886],
        'name': 'KRASKÓW',
        'value': 0.8
    },
    {
        'position': [50.78194444444444, 16.584166666666665],
        'name': 'MOŚCISKO',
        'value': 0.8
    },
    {
        'position': [50.72583333333333, 16.64611111111111],
        'name': 'DZIERŻONIÓW',
        'value': 0.8
    },
    {
        'position': [50.73305555555556, 16.403333333333332],
        'name': 'JUGOWICE',
        'value': 0.8
    },
    { 'position': [51.0375, 16.985], 'name': 'ŚLĘZA', 'value': 0.8 },
    {
        'position': [50.80222222222222, 16.892222222222223],
        'name': 'BIAŁOBRZEZIE',
        'value': 0.8
    },
    {
        'position': [50.94638888888888, 17.30638888888889],
        'name': 'OŁAWA',
        'value': 0.8
    },
    {
        'position': [51.08222222222223, 17.146944444444443],
        'name': 'TRESTNO',
        'value': 0.8
    },
    {
        'position': [50.86555555555556, 17.46944444444444],
        'name': 'BRZEG',
        'value': 0.8
    }]

station_names = [station['name'] for station in stations]

hydro_data = hydro_data[hydro_data['station_name'].isin(station_names)]

hydro_data_filtered = hydro_data[hydro_data['water_level_cm'] != 9999]
hydro_data_grouped = hydro_data_filtered.groupby('station_name')

hydro_data_mean_values = hydro_data_grouped.mean(numeric_only=True)["water_level_cm"]
hydro_data_std_values = hydro_data_grouped.std(numeric_only=True)["water_level_cm"]

deltas = (hydro_data[["water_level_cm"]] - hydro_data.shift(1)[["water_level_cm"]]) / hydro_data.shift(1)[["water_level_cm"]]
deltas["station_name"] = hydro_data["station_name"]
deltas["date"] = hydro_data["date"]

deltas_filtered = deltas[deltas['water_level_cm'] != 9999]

deltas_filtered_std = deltas_filtered.groupby('station_name').std(numeric_only=True)["water_level_cm"].to_dict()

@app.get("/station_values")
async def get_stations(date: str = None):
    #print("####")
    data_for_day = deltas[deltas['date'] == date]
    #print("?@@@@@@@")
    res = data_for_day.groupby('station_name').mean(numeric_only=True)["water_level_cm"].fillna(0).to_dict()

    res = {
        name: np.clip(((float(value / deltas_filtered_std.get(name, 1)) + 1) / 2), 0, 1) if pd.notna(float(value / deltas_filtered_std.get(name, 1))) else 0
        for name, value in res.items()
    }

    print(res)

    return res

@app.get("/get_rainfall")
async def get_rainfall(year: int, month: int, day: int):
    start_date = dt.datetime(year, month, day)
    rains = []
    for i in range(14):
        rain = real_data.loc[real_data.index == start_date, "251160080_rainfall_mm_1"].fillna(0)[0]
        start_date = start_date + dt.timedelta(days=1)
        rains.append({'x': str(start_date)[:10], 'y': float(rain)})

    return rains

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8123)